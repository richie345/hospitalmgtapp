package com.example.hospitalmgt.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hospitalmgt.Adapters.DiagnosisAdapter;
import com.example.hospitalmgt.Helpers.SQLDBHelper;
import com.example.hospitalmgt.Helpers.SessionManager;
import com.example.hospitalmgt.Models.Diagnosis;
import com.example.hospitalmgt.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Home extends AppCompatActivity implements DiagnosisAdapter.DiagnosisAdapterListener {
    private SessionManager session;
    SQLiteOpenHelper openhelper;
    SQLiteDatabase db;
    ImageView back;
    TextView textView3, textView4;
    SearchView searchView;
    private RecyclerView simple_recycler;
    private static final String TAG = MainActivity.class.getSimpleName();
    private RecyclerView recyclerView;
    private List<Diagnosis> diagnosisList;
    private DiagnosisAdapter rAdapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        // Session manager
        session = new SessionManager(getApplicationContext());

        openhelper=new SQLDBHelper(Home.this);
        db=openhelper.getWritableDatabase();

        diagnosisList = getDiagnosis();

        simple_recycler=(RecyclerView) findViewById(R.id.mRecycler);
        rAdapter = new DiagnosisAdapter(this, diagnosisList, this);
        back = (ImageView) findViewById(R.id.backarrow);
        searchView = (SearchView) findViewById(R.id.filter); // inititate a search view
        CharSequence queryHint = searchView.getQueryHint(); // get the query string currently in the text field

        layoutManager = new LinearLayoutManager(Home.this); //Create a Layout Manager

        simple_recycler.setVisibility(View.VISIBLE);
        simple_recycler.setHasFixedSize(true);
        simple_recycler.setLayoutManager(layoutManager);
        simple_recycler.setAdapter(rAdapter);

        // listening to search query text change
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                rAdapter.getFilter().filter(query);
                //return true;
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                rAdapter.getFilter().filter(query);
                //return true;
                return false;
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this, Home.class);
                startActivity(intent);
                finish();
            }
        });

    }

    public List<Diagnosis> getDiagnosis() {
        List<Diagnosis> benList = new ArrayList<Diagnosis>();
        // Select All Query
        String selectQuery = "SELECT title, patient_id,presenting_complain,carried_out_by,date_carried_out,remarks FROM " + SQLDBHelper.TABLE_DIAGNOSIS+ ";";
        SQLiteDatabase dbase = openhelper.getReadableDatabase();
        Cursor cursor = dbase.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                Diagnosis ben = new Diagnosis();
                ben.setTitle(cursor.getString(0));
                ben.setPatient_id(cursor.getString(1));
                ben.setComplaint(cursor.getString(2));
                ben.setCarried_out_by(cursor.getString(3));
                ben.setDate_carried_out(new Date(cursor.getString(4)));
                ben.setRemarks(cursor.getString(5));

                benList.add(ben);
            } while (cursor.moveToNext());
        }
        // refreshing recycler view
//        rAdapter.notifyDataSetChanged();
        //cursor.close();
        //dbase.close();
        // return ben list
        return benList;
    }

    @Override
    public void onContactSelected(Diagnosis contact) {
        Toast.makeText(Home.this, "Selected: " + contact.getTitle(), Toast.LENGTH_LONG).show();

    }

}