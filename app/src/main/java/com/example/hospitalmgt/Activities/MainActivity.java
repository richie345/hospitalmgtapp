package com.example.hospitalmgt.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.hospitalmgt.App.AppConfig;
import com.example.hospitalmgt.App.AppController;
import com.example.hospitalmgt.Helpers.SQLDBHelper;
import com.example.hospitalmgt.Helpers.SessionManager;
import com.example.hospitalmgt.R;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    TextInputEditText username,pass;
    Button submit;
    private ProgressDialog pDialog;
    private SessionManager session;
    SQLDBHelper db;
    Cursor cursor;
    SQLiteDatabase sqLiteDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        username = (TextInputEditText) findViewById(R.id.editTextEmail);
        pass = (TextInputEditText) findViewById(R.id.editTextPassword);
        submit = (Button) findViewById(R.id.cirLoginButton);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // Session manager
        session = new SessionManager(getApplicationContext());

        // SQLite database handler
        db = new SQLDBHelper(getApplicationContext());
        sqLiteDatabase = db.getReadableDatabase();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uid = username.getText().toString().trim();
                String password = pass.getText().toString().trim();
                if (!uid.isEmpty() && !password.isEmpty()) {
                    if(uid.equals("admin") && password.equals("1234")) {
                        if (checkNetwork()) {
                            getDiagnosis();
                        } else {
                            Toast.makeText(MainActivity.this, "Please turn on your INTERNET CONNECTION and try again!", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    Toast.makeText(MainActivity.this, "Incorrect Username or Password", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void getDiagnosis() {
        // Tag used to cancel the request
        String tag_string_req = "req_register";

        pDialog.setMessage("Getting Diagnosis ...");
        showDialog();
        Log.e(TAG, "1==");
        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.GET_DIAGNOSIS,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "Diagnosis Response: " + response.toString());
                        hideDialog();

                        try {
                            JSONObject jObj = new JSONObject(response);
                            if (response != null) {
                                // User successfully stored in MySQL
                                // Now store the user in sqlite
                                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

                                if (jObj.length()>0) {
                                    Date createdAt = null;
                                    Date updatedAt = null;
                                    Date deletedAt = null;
                                    for (int i = 0; i < jObj.length(); i++) {
                                        //JSONObject jobj_ben = jObj.getJSONObject(i);
                                        JSONArray jArray2 = new JSONArray(jObj.getString("data"));
                                        if (jArray2.length()>0) {
                                            for (int j = 0; j < jArray2.length(); j++) {
                                                JSONObject jobj_ben2 = jArray2.getJSONObject(j);
                                                Cursor cursor = sqLiteDatabase.rawQuery("select * from" +
                                                        " " + SQLDBHelper.TABLE_DIAGNOSIS + " ", null);
                                                if (cursor.getCount() == 0) {
                                                    // Inserting row in diagnosis table
                                                    Date date_carried_out = null;
                                                    date_carried_out = dateFormat.parse(jobj_ben2.getString("date_carried_out"));
                                                    //createdAt = dateFormat.parse(jobj_ben2.getString("created_at"));
                                                    //updatedAt = dateFormat.parse(jobj_ben2.getString("updated_at"));
                                                    db.addDiagnosis(jobj_ben2.getString("title"),
                                                            jobj_ben2.getString("patient_id"), jobj_ben2.getString("presenting_complain"),
                                                            jobj_ben2.getString("history_of_present_illness"), jobj_ben2.getString("medications"),
                                                            jobj_ben2.getString("allergies"),
                                                            jobj_ben2.getString("past_medical_history"),
                                                            jobj_ben2.getString("family_history"),
                                                            jobj_ben2.getString("social_history"),
                                                            jobj_ben2.getString("drug_history"),
                                                            jobj_ben2.getString("occupational_history"),
                                                            jobj_ben2.getString("review_of_systems"), jobj_ben2.getString("physical_mental_examination"),
                                                            jobj_ben2.getString("lab"), jobj_ben2.getString("assessment"), jobj_ben2.getString("carried_out_by"),
                                                            jobj_ben2.getString("plan"), jobj_ben2.getString("entry_id"), date_carried_out,
                                                            jobj_ben2.getString("remarks"), jobj_ben2.getString("valid"), createdAt, updatedAt
                                                    );
                                                    System.out.println("inserted to diagnosis...");
                                                    Intent aInt = new Intent(MainActivity.this, Home.class);
                                                    startActivity(aInt);
                                                    finish();
                                                }
                                            }

                                        }else{
                                            continue;
                                        }

                                    }
                                }
                                Intent aInt = new Intent(MainActivity.this,Home.class);
                                startActivity(aInt);
                                finish();
                                Log.e(TAG,"moving to home");

                            } else {

                                // Error occurred in registration. Get the error
                                // message
                                String errorMsg = "Error 419"; //jObj.getString("error_msg");
                                Toast.makeText(getApplicationContext(),
                                        errorMsg, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException | ParseException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Payment Batch Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                //hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    //Check network availability
    public boolean checkNetwork(){
        ConnectivityManager connectivityManager;
        connectivityManager= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}