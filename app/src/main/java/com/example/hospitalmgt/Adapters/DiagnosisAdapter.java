package com.example.hospitalmgt.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.example.hospitalmgt.Models.Diagnosis;
import com.example.hospitalmgt.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class DiagnosisAdapter extends RecyclerView.Adapter<DiagnosisAdapter.MyViewHolder>
        implements Filterable {
    private Context context;
    private List<Diagnosis> contactList;
    private List<Diagnosis> contactListFiltered;
    private DiagnosisAdapterListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, patientid, carriedoutby, complaint, remarks;
        public CardView cardView;

        public MyViewHolder( @NonNull View view) {
            super(view);
            title = view.findViewById(R.id.tv_title);
            patientid= view.findViewById(R.id.tv_patient);
            carriedoutby= view.findViewById(R.id.tv_carriedoutby);
            complaint= view.findViewById(R.id.tv_complaint);
            remarks= view.findViewById(R.id.tv_remark);
            //cardView = view.findViewById(R.id.cardView);

            /*cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // send selected contact in callback
                    listener.onContactSelected(contactListFiltered.get(getAdapterPosition()));
                }
            });*/
        }

    }


    public DiagnosisAdapter(Context context, List<Diagnosis> contactList, DiagnosisAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.contactList = contactList;
        this.contactListFiltered = contactList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_visit_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Diagnosis contact = contactListFiltered.get(position);
        holder.title.setText(contact.getTitle());
        holder.patientid.setText(contact.getPatient_id());
        holder.carriedoutby.setText(contact.getCarried_out_by());
        holder.complaint.setText(contact.getComplaint());
        holder.remarks.setText(contact.getRemarks());

        /*holder.firstName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onContactSelected(contactListFiltered.get(position));
            }
        });*/

    }

    @Override
    public int getItemCount() {
        return contactListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    contactListFiltered = contactList;
                } else {
                    List<Diagnosis> filteredList = new ArrayList<>();
                    for (Diagnosis row : contactList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getTitle().toLowerCase().contains(charString.toLowerCase()) || row.getPatient_id().contains(charSequence)
                                || row.getCarried_out_by().contains(charSequence) || row.getComplaint().contains(charSequence)
                                || row.getRemarks().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    contactListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = contactListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                contactListFiltered = (ArrayList<Diagnosis>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface DiagnosisAdapterListener {
        void onContactSelected(Diagnosis position);
    }

}
