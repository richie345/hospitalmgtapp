package com.example.hospitalmgt.Models;

import java.util.Date;

public class Diagnosis {
    String title;
    String patient_id;
    String complaint;
    String history_present_illness;
    String medications;
    String allergies; String past_medical_history; String family_history;
    String social_history;
    String drug_history; String occupational_history;
    String review_of_systems;
    String physical_mental_examination;
    String lab;
    String assessment;
    String carried_out_by;
    String plan;
    String entry_id;
    Date date_carried_out;
    String remarks;
    String valid;
    Date created_at;
    Date updated_at;

    public Diagnosis() {
    }

    public Diagnosis(String title, String patient_id, String complaint, String carried_out_by, Date date_carried_out, String remarks, String valid) {
        this.title = title;
        this.patient_id = patient_id;
        this.complaint = complaint;
        this.carried_out_by = carried_out_by;
        this.date_carried_out = date_carried_out;
        this.remarks = remarks;
        this.valid = valid;
    }

    public Diagnosis(String title, String patient_id, String complaint, String history_present_illness, String medications, String allergies, String past_medical_history, String family_history, String social_history, String drug_history, String occupational_history, String review_of_systems, String physical_mental_examination, String lab, String assessment, String carried_out_by, String plan, String entry_id, Date date_carried_out, String remarks, String valid, Date created_at, Date updated_at) {
        this.title = title;
        this.patient_id = patient_id;
        this.complaint = complaint;
        this.history_present_illness = history_present_illness;
        this.medications = medications;
        this.allergies = allergies;
        this.past_medical_history = past_medical_history;
        this.family_history = family_history;
        this.social_history = social_history;
        this.drug_history = drug_history;
        this.occupational_history = occupational_history;
        this.review_of_systems = review_of_systems;
        this.physical_mental_examination = physical_mental_examination;
        this.lab = lab;
        this.assessment = assessment;
        this.carried_out_by = carried_out_by;
        this.plan = plan;
        this.entry_id = entry_id;
        this.date_carried_out = date_carried_out;
        this.remarks = remarks;
        this.valid = valid;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(String patient_id) {
        this.patient_id = patient_id;
    }

    public String getComplaint() {
        return complaint;
    }

    public void setComplaint(String complaint) {
        this.complaint = complaint;
    }

    public String getHistory_present_illness() {
        return history_present_illness;
    }

    public void setHistory_present_illness(String history_present_illness) {
        this.history_present_illness = history_present_illness;
    }

    public String getMedications() {
        return medications;
    }

    public void setMedications(String medications) {
        this.medications = medications;
    }

    public String getAllergies() {
        return allergies;
    }

    public void setAllergies(String allergies) {
        this.allergies = allergies;
    }

    public String getPast_medical_history() {
        return past_medical_history;
    }

    public void setPast_medical_history(String past_medical_history) {
        this.past_medical_history = past_medical_history;
    }

    public String getFamily_history() {
        return family_history;
    }

    public void setFamily_history(String family_history) {
        this.family_history = family_history;
    }

    public String getSocial_history() {
        return social_history;
    }

    public void setSocial_history(String social_history) {
        this.social_history = social_history;
    }

    public String getDrug_history() {
        return drug_history;
    }

    public void setDrug_history(String drug_history) {
        this.drug_history = drug_history;
    }

    public String getOccupational_history() {
        return occupational_history;
    }

    public void setOccupational_history(String occupational_history) {
        this.occupational_history = occupational_history;
    }

    public String getReview_of_systems() {
        return review_of_systems;
    }

    public void setReview_of_systems(String review_of_systems) {
        this.review_of_systems = review_of_systems;
    }

    public String getPhysical_mental_examination() {
        return physical_mental_examination;
    }

    public void setPhysical_mental_examination(String physical_mental_examination) {
        this.physical_mental_examination = physical_mental_examination;
    }

    public String getLab() {
        return lab;
    }

    public void setLab(String lab) {
        this.lab = lab;
    }

    public String getAssessment() {
        return assessment;
    }

    public void setAssessment(String assessment) {
        this.assessment = assessment;
    }

    public String getCarried_out_by() {
        return carried_out_by;
    }

    public void setCarried_out_by(String carried_out_by) {
        this.carried_out_by = carried_out_by;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public String getEntry_id() {
        return entry_id;
    }

    public void setEntry_id(String entry_id) {
        this.entry_id = entry_id;
    }

    public Date getDate_carried_out() {
        return date_carried_out;
    }

    public void setDate_carried_out(Date date_carried_out) {
        this.date_carried_out = date_carried_out;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getValid() {
        return valid;
    }

    public void setValid(String valid) {
        this.valid = valid;
    }
}
