package com.example.hospitalmgt.Helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


/**
 * Created by EMOCHE on 21-Aug-20.
 */

public class SQLDBHelper extends SQLiteOpenHelper {
    private static final String TAG = SQLDBHelper.class.getSimpleName();

    private static String DATABASE_NAME="hospitalmgt1.db";
    private static int DATABASE_VERSION=1;
    private SQLiteDatabase db;

    String createDiagnosis="create table diagnosis(id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT," +
            " patient_id TEXT, presenting_complain TEXT, history_of_present_illness TEXT, medications TEXT, allergies TEXT," +
            " past_medical_history TEXT, family_history TEXT, social_history TEXT, drug_history TEXT," +
            " occupational_history TEXT, review_of_systems TEXT, physical_mental_examination TEXT," +
            " lab TEXT, assessment TEXT, carried_out_by TEXT, plan1 TEXT, entry_id TEXT, date_carried_out DATE," +
            " remarks TEXT, valid TEXT, created_at DATE, updated_at DATE)";



    public static String TABLE_DIAGNOSIS="diagnosis";

    public SQLDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Storing diagnosis details in database
     */
    public void addDiagnosis(String title, String patient_id, String complaint, String history_present_illness,
                               String medications, String allergies, String past_medical_history, String family_history, String social_history,
                               String drug_history, String occupational_history, String review_of_systems, String physical_mental_examination,
                               String lab, String assessment, String carried_out_by, String plan, String entry_id,
                               Date date_carried_out, String remarks, String valid, Date created_at, Date updated_at) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("title", title); // Name
        values.put("patient_id", patient_id); // Email
        values.put("date_carried_out", date_carried_out.toString()); // Created At
        values.put("presenting_complain", complaint); // Created At
        values.put("history_of_present_illness", history_present_illness); // Created At
        values.put("medications", medications); // Created At
        values.put("allergies", allergies); // Created At
        values.put("past_medical_history", past_medical_history); // Created At
        values.put("family_history", family_history);
        values.put("social_history", social_history);
        values.put("drug_history", drug_history);
        values.put("occupational_history", occupational_history);
        values.put("review_of_systems", review_of_systems);
        values.put("physical_mental_examination", physical_mental_examination);
        values.put("lab", lab);
        values.put("assessment", assessment);
        values.put("carried_out_by", carried_out_by);
        values.put("plan1", plan);
        values.put("entry_id", entry_id);
        values.put("remarks", remarks);
        values.put("valid", valid);
        values.put("created_at", ""); // Created At
        values.put("updated_at", ""); // UPdated At

        // Inserting Row
        long id = db.insert(TABLE_DIAGNOSIS, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New diagnosis inserted into sqlite: " + id);
    }

    public void truncateTables() {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_DIAGNOSIS, null, null);

        db.close();
        Log.d(TAG, "Deleted all user info from sqlite");
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        db=sqLiteDatabase;
        sqLiteDatabase.execSQL(createDiagnosis);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("drop table " + TABLE_DIAGNOSIS);
        onCreate(sqLiteDatabase);
    }

    @Override
    protected void finalize() throws Throwable {
        this.close();
        super.finalize();
    }
}
